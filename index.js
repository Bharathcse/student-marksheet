const fastify = require('fastify')({
    logger: false
  })

// Run the server!
fastify.listen(3010, (err, address) => {
    if (err) {
        fastify.log.error(err)
        process.exit(1)
    }
    console.log(`server listening on ${address}`)
})

const studentMap = new Map();

fastify.route({
    method: 'GET',
    url: '/report',
    schema: {},
    handler: async (req, res) => {
        let studentArr = [];
        for (let [key, value] of studentMap) {
            studentArr.push(studentToJSON(value));
        }
        res.send(JSON.stringify(studentArr));
    }
})

fastify.route({
    method: 'POST',
    url: '/add',
    schema: {
        querystring: {
            type: 'object',
            properties: {
                studentName: { type: 'string' },
                studentID: { type: 'string' },
                subject1: { type: 'integer' },
                subject2: { type: 'integer' },
                subject3: { type: 'integer' },
                subject4: { type: 'integer' },
                subject5: { type: 'integer' },
            },
            required: ['studentName', 'studentID', 'subject1', 'subject2', 'subject3', 'subject4', 'subject5']
        }
    },
    handler: async (req, res) => {
        let student = new Student(req.query.studentName, req.query.studentID, req.query.subject1, req.query.subject2, 
            req.query.subject4, req.query.subject4, req.query.subject5);
        studentMap.set(student.studentID, student);
        console.log(`Student added successfully ${student.studentID}`)
        res.send(JSON.stringify(studentToJSON(student)));
    }
})

fastify.route({
    method: 'PUT',
    url: '/update',
    schema: {
        querystring: {
            type: 'object',
            properties: {
                studentID: { type: 'string' },
                subject1: { type: 'integer' },
                subject2: { type: 'integer' },
                subject3: { type: 'integer' },
                subject4: { type: 'integer' },
                subject5: { type: 'integer' },
            },
            required: ['studentID']
        }
    },
    handler: async (req, res) => {
        if (!studentMap.has(req.query.studentID)) {
            return res.status(400).send(new Error('Invalid studentID'));
        }
        let student = studentMap.get(req.query.studentID);
        if (req.query.subject1) {
            student.subject1 = req.query.subject1;
        } else if (req.query.subject2) {
            student.subject2 = req.query.subject2;        
        } else if (req.query.subject3) {
            student.subject3 = req.query.subject3;        
        } else if (req.query.subject4) {
            student.subject4 = req.query.subject4;        
        } else if (req.query.subject5) {
            student.subject5 = req.query.subject5;        
        }
        studentMap.set(student.studentID, student);
        console.log(`Student updated successfully ${student.studentID}`)
        res.send(JSON.stringify(studentToJSON(student)));
    }
})

fastify.route({
    method: 'DELETE',
    url: '/delete',
    schema: {
        querystring: {
            type: 'object',
            properties: {
                studentName: { type: 'string' }
            },
            required: ['studentID']
        }
    },
    handler: async (req, res) => {
        if (!studentMap.has(req.query.studentID)) {
            return res.status(400).send(new Error('Invalid studentID'));
        }
        studentMap.delete(req.query.studentID);
        console.log(`Student deleted successfully ${req.query.studentID}`)
        return res.send({'message' : `Student deleted successfully ${req.query.studentID}`});
    }
})


function studentToJSON(student) {
    const jsonOutput = {
        studentName: student.studentName,
        studentID: student.studentID,
        subject1: student.subject1,
        subject2: student.subject2,
        subject3: student.subject3,
        subject4: student.subject4,
        subject5: student.subject5,
    }
    return jsonOutput;
}

class Student {
    constructor(name, id, sub1, sub2, sub3, sub4, sub5) {
        this.studentName = name;
        this.studentID = id;
        this.subject1 = sub1;
        this.subject2 = sub2;
        this.subject3 = sub3;
        this.subject4 = sub4;
        this.subject5 = sub5;
    }
}
