## Students Marksheet

# Get all student details

Get the details of the stored student details

**URL** : `/report`

**Method** : `GET`

## Success Response

**Code** : `200 OK`

**Sample output**


```json
[
    {
        "studentName": "test",
        "studentID": "1",
        "subject1": 11,
        "subject2": 2,
        "subject3": 4,
        "subject4": 4,
        "subject5": 5
    },
    {
        "studentName": "test1",
        "studentID": "2",
        "subject1": 11,
        "subject2": 2,
        "subject3": 4,
        "subject4": 4,
        "subject5": 5
    }
]
```

# Add student details

To stored student details

**URL** : `/add`

**Method** : `POST`

## Success Response

**Code** : `200 OK`

**Sample output**


```json
{
    "studentName": "test1",
    "studentID": "2",
    "subject1": 11,
    "subject2": 2,
    "subject3": 4,
    "subject4": 4,
    "subject5": 5
}
```

# Update student details

To update particular student details

**URL** : `/update`

**Method** : `PUT`

## Success Response

**Code** : `200 OK`

**Sample output**


```json
{
    "studentName": "test1",
    "studentID": "2",
    "subject1": 100,
    "subject2": 2,
    "subject3": 4,
    "subject4": 4,
    "subject5": 5
}
```

# Delete student details

To delete particular student details

**URL** : `/delete`

**Method** : `DELETE`

## Success Response

**Code** : `200 OK`

**Sample output**


```json
{
    "message": "Student deleted successfully 2"
}
```
